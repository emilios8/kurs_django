from django.db import models
from django.contrib.auth.models import User
from shelf.models import BookItem

from django.utils.timezone import now

class Rental(models.Model): # Będzie przechowywał wypożyczenia i zwroty od naszych użytkowników
    who = models.ForeignKey(User)
    what = models.ForeignKey(BookItem)
    when = models.DateTimeField(default=now)
    returned = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return ''    # Zadanie domowe :) Sprawdzić działanie wypożyczania w panelu admina.