from django.shortcuts import render
from django.views.generic import FormView, DetailView
from .models import Message
from .forms import MessageForm, ContactForm


# class MessageDetailView(DetailView):
#     model = Message


class MessageAddView(FormView):
    form_class = ContactForm
    template_name = 'contact/message_form.html'
    success_url = '/contact'


    # def form_valid(self, form):
    #     form.save() # boform jest instancją ModelForm, który posiada metodę save()
    #     return super(MessageAddView, self).form_valid(form)