from django.db import models

class Message(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField()
    message = models.TextField()  # Pole tekstowe o teoretycznie nieograniczonej długości


    def __str__(self):
        return "message from (name)".format(name=self.name)