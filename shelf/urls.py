from django.urls import path
from shelf.views import AuthorListView, AuthorDetailView


app_name = 'shelf'
urlpatterns = [
    path('', AuthorListView.as_view(), name='author-list'),
    path('<int:pk>', AuthorDetailView.as_view(), name='author-detail'),
]