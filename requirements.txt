Django==2.1.4
django-configurations==2.1
Pillow==5.4.0
pkg-resources==0.0.0
pytz==2018.7
